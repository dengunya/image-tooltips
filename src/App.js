import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Redirect, IndexRoute, browserHistory } from 'react-router';
import Root from './components/Root';
import Index from './components/pages/Index';
import About from './components/pages/About';
import Image from './components/pages/Image';

render(
    (<Router history={browserHistory}>
        <Route path="/" component={Root}>
            <IndexRoute component={Index} />
            <Route path="/image/:imageID" component={Image} />
            <Route path="/about" component={About} />
        </Route>
        <Redirect from='*' to='/' />
    </Router>), document.getElementById('root')
);