import Rx from 'rxjs';
import Request from '../api/images';

let imagesSubjects = {
    fetchServerImages: new Rx.Subject(),
    addTooltipSubject: new Rx.Subject(),
    removeTooltipSubject: new Rx.Subject()
};

export default {
    imagesSubjects,
    fetchServerImages: () => {
        return Request.get().subscribe(::imagesSubjects.fetchServerImages.next);
    },
    addTooltip: (data) => imagesSubjects.addTooltipSubject.next(data),
    removeTooltip: (data) => imagesSubjects.removeTooltipSubject.next(data)
}
