import Rx from 'rxjs';
import update from 'immutability-helper';

import IntentImages from '../intent/IntentImages';

const subject = new Rx.ReplaySubject(1);

let state = {
    counter: 0,
    mode: 'edit',
    images: []
};

IntentImages.imagesSubjects.fetchServerImages.subscribe((data) => {
    state = update(state, {
        $merge: {
            images: data.data
        }
    });
    subject.next(state);
});

IntentImages.imagesSubjects.addTooltipSubject.subscribe((data) => {
    let images = ([...state.images]);
    images[data.imageID].tips.push({position: data.position, text: data.text});
    state = update(state, {
        $merge: {
            images: images
        }
    });
    subject.next(state);
});

IntentImages.imagesSubjects.removeTooltipSubject.subscribe((data) => {
    let images = ([...state.images]);
    images[data.imageID].tips.splice(data.tipID, 1);
    state = update(state, {
        $merge: {
            images: images
        }
    });
    subject.next(state);
});


subject.next(state);

export default {
    subject
};