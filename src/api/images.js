import Rx from 'rxjs';
import axios from 'axios';

const apiUrl = 'http://127.0.0.1:1337';

const buildRequest = (url, params) =>
    Rx.Observable.fromPromise(
        axios.get(url)
    );

export default {
    get: () => buildRequest(apiUrl + '/images')
}