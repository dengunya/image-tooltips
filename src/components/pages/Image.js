import React, {Component} from 'react';
import { browserHistory } from 'react-router';
import IntentImages from '../../intent/IntentImages';
import Pointer from '../partial/Pointer';

 class Image extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const imageID = this.props.params.imageID*1;

        this.imageID = imageID;

        if (!this.props.images || !this.props.images.length || !this.props.images[imageID]) {
            this.image = null;
        }
        else {
            this.image = this.props.images[imageID];
        }
    }

    imageClicked(e) {
        let
            event = e.nativeEvent,
            target = event.target,
            size = {w: target.clientWidth, h: target.clientHeight},
            clickPosition = {x: event.layerX - 20, y: event.layerY - 20}, // -20 fastest but not the best way to calculate pointer position
            position = {
                x: clickPosition.x/size.w,
                y: clickPosition.y/size.h
            },
            text = this.tooltipTextInput.value,
            imageID = this.props.params.imageID*1
        ;//

        if (target.nodeName === 'IMG') {
            IntentImages.addTooltip({imageID, position, text});
            this.tooltipTextInput.value = 'Sample value';
        }
    }

    saveButtonClicked() {}

    render() {

        const image = this.image;

        if (image) {
            return (
                <div className="row image-page-wrapper">
                    <div className="col-md-6">
                        <div className="image-wrapper" onClick={::this.imageClicked}>
                            <img src={image.src} />
                            {image.tips.map((tip, index) => <Pointer key={index} imageID={this.imageID} tipID={index} data={tip} />)}
                        </div>
                    </div>
                    <div className="col-md-6">
                        <label>Enter tooltip text: </label>
                        <input ref={(input) => { this.tooltipTextInput = input; }} />
                        <br/>
                        <br/>
                        <bitton className="btn btn-default" onClick={::this.saveButtonClicked}>Save</bitton>
                    </div>
                </div>
            );
        }
        else {
            return (<div>...</div>);
        }
    }

}

export default Image;