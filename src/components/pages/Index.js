import React, {Component} from 'react';
import IntentImages from '../../intent/IntentImages';
import ImageListItem from '../partial/ImageListItem';

export default class Index extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (!this.props.images || !this.props.images.length) {
            IntentImages.fetchServerImages();
        }
    }

    render() {
        return (
            <div>
                <h2>Index</h2>

                <hr/>

                <ul>
                    {(this.props.images) ? this.props.images.map((image, index) => (<ImageListItem index={index} key={index} image={image} />)) : null}
                </ul>

                <hr/>

            </div>
        )
    }

};
