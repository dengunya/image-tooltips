import React from 'react';
import {Link} from 'react-router';

const ImageListItem = (props) => (
    <li className="row">
        <Link to={`/image/${props.index}`}>
            <div className="col-md-3">
                <img src={props.image.src} />
            </div>
            <div className="col-md-9">
                {props.image.src}
            </div>
        </Link>
    </li>
);

export default ImageListItem;
