import React, {Component} from 'react';
import IntentImages from '../../intent/IntentImages';

export default class Pointer extends Component {

    constructor(props) {
        super(props);
    }

    pointerClicked() {
        const tipID = this.props.tipID;
        const data = this.props.data;
        IntentImages.removeTooltip(this.props);
    }

    render() {
        return (
            <div onClick={::this.pointerClicked} className="pointer" style={{
                left: this.props.data.position.x*100 + '%',
                top: this.props.data.position.y*100 + '%'
            }}>
                <div className="pointer-content">
                    <div className="pointer-tooltip">
                        <span>{this.props.data.text}</span>
                    </div>
                </div>
            </div>
        );
    }

}