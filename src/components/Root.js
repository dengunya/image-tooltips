import React, {Component} from 'react';
import { Link } from 'react-router';
import { version } from '../../package.json';
import { default as State } from '../store/State';

export default class Root extends Component {

    componentDidMount() {
        State.subject.subscribe(appState => {
            this.setState({ ...appState });
        });
    }

    render() {
        return (
            <div>

                <nav className="navbar navbar-inverse">
                    <div className="container">
                        <div className="navbar-header">
                            <Link className="navbar-brand" to="/">
                                React Rx Application {version}
                            </Link>
                        </div>
                        <div id="navbar" className="collapse navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li><Link to="/about">About</Link></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container">
                    <section>
                        {React.cloneElement(this.props.children, { ...this.state })}
                    </section>
                </div>
            </div>
        );
    }
}