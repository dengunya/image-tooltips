import http from 'http';
import express from 'express';

const app = express();

let initialState = {
    images: [
        {
            "src": "https://im0-tub-ua.yandex.net/i?id=df5093f64852ca9d8f97c13358a5ee70-l&n=13",
            "tips": [
                {
                    position: {
                        x: 0.609009009009009009,
                        y: 0.16025641025641024
                    },
                    text: 'Some text'
                },
                {
                    position: {
                        x: 0.059009009009009009,
                        y: 0.336025641025641024
                    },
                    text: 'Some text'
                },
            ]
        },
        {
            "src": "http://gotolow.com/assets/addons/low-random.jpg",
            "tips": []
        }
    ]
};

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/images', function(req, res) {
    res.send(initialState.images);
});

// Start server
app.listen(1337);